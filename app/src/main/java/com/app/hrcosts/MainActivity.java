package com.app.hrcosts;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import androidx.appcompat.widget.SwitchCompat;
import androidx.core.view.GravityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

/*    EditText ral;
    TextView rml;
    TextView costIrpef;
    TextView irpefCl;
    TextView icl;
    TextView annual;
    TextView daily;
    TextView birpef;
    TextView nim;
    TextView tvTfr;
    TextView tvTestPeriod;
    TextView alertMinRml;
    TextView tvRalInpsCa;

    TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            clearDetail();
        }
    };*/

    @Override
    protected void onStart() {
        super.onStart();
        updateUi();
    }

 /*   Spinner contract;
    Spinner level;
    ImageView alertImg;
    Button btDetails;*/

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_commercio, R.id.nav_meccanico, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    /*public void CalculateCostsTi(View view) {

        ral = findViewById(R.id.et_ral);

        if (ral.getText().toString().length() == 0){
            ral.setText("0");

        } else {ral.addTextChangedListener(tw);}

        double ralDouble = Double.parseDouble(ral.getText().toString());
        int ralInt = (int) ralDouble;
        String ralStr = String.valueOf(ralInt);
        double irpef = 0;
        double fissoDetrazione = 0;

        rml = findViewById(R.id.tv_rml);
        costIrpef = findViewById(R.id.tv_costanteIrpef);
        irpefCl = findViewById(R.id.tv_irpefcl);
        icl = findViewById(R.id.tv_inpscl);
        annual = findViewById(R.id.tv_annual_cost);
        daily = findViewById(R.id.tv_daily_cost);
        birpef = findViewById(R.id.tv_baseirpef);
        nim = findViewById(R.id.tv_nim);
        tvTfr = findViewById(R.id.tv_tfr);
        tvTestPeriod = findViewById(R.id.tv_test_period);
        alertMinRml = findViewById(R.id.tv_alert_rml_min);
        tvRalInpsCa = findViewById(R.id.tv_ralinps_ca);
        contract = findViewById(R.id.sp_contract);
        level = findViewById(R.id.sp_level);
        alertImg = findViewById(R.id.iv_alertImg);
        btDetails = findViewById(R.id.bt_details);

        contract.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                clearDetail();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        level.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                clearDetail();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });


        if (ralInt < 8000) {
            Toast.makeText(this,"Inserire un RAL che sia degno di un lavoratore",Toast.LENGTH_SHORT).show();
            clearAll();
        } else {

            if (ralDouble >= 999 && ralDouble <= 15000) {
                irpef = (0.23);
            }else if (ralDouble >= 15001 && ralDouble <= 28000) {
                irpef = (0.27);
                fissoDetrazione = 600;
            }else if (ralDouble >= 28001 && ralDouble <= 55000) {
                irpef = (0.38);
                fissoDetrazione = 3680;
            }else if (ralDouble >= 55001 && ralDouble <= 75000) {
                irpef = (0.41);
                fissoDetrazione = 5330;
            }else if (ralDouble >= 75001) {
                irpef = (0.43);
                fissoDetrazione = 6830;
            }

            double rmlDouble = ralDouble / 14;
            double inpsClInt = ralDouble * 0.0949;
            double tfrDouble = ralDouble / 13.5;
            double ralInpsCaDouble = 0;
            String contracts = contract.getSelectedItem().toString();
            switch (contracts) {
                case "Indeterminato":
                    ralInpsCaDouble = ralDouble * 1.2936;
                    break;
                case "Determinato":
                    ralInpsCaDouble = ralDouble * 1.3076;
                    break;
                case "Apprendistato":
                    ralInpsCaDouble = ralDouble * 1.1;
            }
            //double ralInpsCaDouble = ralDouble * 1.2936;
            //double annualCost = (ralDouble * 1.39);
            double annualCost = (ralInpsCaDouble + tfrDouble);
            double dailyCost = annualCost / 210;
            double baseIrpef = ralDouble - inpsClInt;
            double irpefClInt = (baseIrpef * irpef) - fissoDetrazione;
            double nimDouble = (baseIrpef-irpefClInt)/14;
            double rmlMin = 0;


            String rmlFormat = new DecimalFormat("##.##").format(rmlDouble);
            String inpsclFormat = new DecimalFormat("##.##").format(inpsClInt);
            String annualCostFormat = new DecimalFormat("##.##").format(annualCost);
            String dailyCostFormat = new DecimalFormat("##.##").format(dailyCost);
            String baseIrpefFormat = new DecimalFormat("##.##").format(baseIrpef);
            String irpefClFormat = new DecimalFormat("##.##").format(irpefClInt);
            String nimFormat = new DecimalFormat("##.##").format(nimDouble);
            String tfrFormat = new DecimalFormat("##.##").format(tfrDouble);
            String ralInpsCaFormat = new DecimalFormat("##.##").format(ralInpsCaDouble);

            String levels = level.getSelectedItem().toString();

            String fissoDetrazioneStr = String.valueOf(fissoDetrazione);
            String nimStr = String.valueOf(nimFormat);

            switch (levels) {
                case "Quadro":
                    rmlMin = 2699.84;
                    break;
                case "1":
                    rmlMin = 2248.08;
                    break;
                case "2":
                    rmlMin = 2012.44;
                    break;
                case "3":
                    rmlMin = 1793.11;
                    break;
                case "4":
                    rmlMin = 1618.75;
                    break;
                case "5":
                    rmlMin = 1510.98;
                    break;
                case "6":
                    rmlMin = 1407.90;
                    break;
                case "7":
                    rmlMin = 1283.36;
                    break;
            }
            String testPeriod = "0";
            switch (levels) {
                case "Quadro":
                case "1":
                    testPeriod = "6 mesi CC";
                    break;
                case "2":
                case "3":
                case "4":
                case "5":
                    testPeriod = "60 giorni LL";
                    break;
                case "6":
                case "7":
                    testPeriod = "45 giorni LL";
                    break;
            }

            if (rmlDouble < rmlMin) {
                alertImg.setImageResource(R.drawable.ic_menu_alert);
                alertImg.setColorFilter(getResources().getColor(R.color.error));
                alertMinRml.setText(String.format("%s%s", getString(R.string.rmlmin), rmlMin));
            } else {
                alertImg.setImageResource(R.drawable.ic_menu_ok);
                alertImg.setColorFilter(getResources().getColor(R.color.teal_700));
                alertMinRml.setText(String.format("%s%s", getString(R.string.rmlok), rmlMin));
            }

            rml.setText(rmlFormat);
            costIrpef.setText(fissoDetrazioneStr);
            icl.setText(inpsclFormat);
            annual.setText(annualCostFormat);
            daily.setText(dailyCostFormat);
            birpef.setText(baseIrpefFormat);
            irpefCl.setText(irpefClFormat);
            nim.setText(nimStr);
            tvRalInpsCa.setText(ralInpsCaFormat);
            tvTfr.setText(tfrFormat);
            tvTestPeriod.setText(testPeriod);

            Toast.makeText(this,"Dati aggiornati",Toast.LENGTH_SHORT).show();
            *//*Snackbar.make(view, "Dati aggiornati", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();*//*

            //Log.i("info", fissoDetrazioneStr);

            btDetails.setVisibility(View.VISIBLE);
        }
    }*/

/*    private void clearDetail() {
        rml.setText("-");
        nim.setText("-");
        tvTfr.setText("-");
        tvTestPeriod.setText("-");
        costIrpef.setText("-");
        birpef.setText("-");
        irpefCl.setText("-");
        icl.setText("-");
        annual.setText("-");
        daily.setText("-");
        alertMinRml.setText("");
        tvRalInpsCa.setText("-");
        alertImg.setImageResource(0);
        btDetails.setVisibility(View.INVISIBLE);
    }
    private void clearAll() {
        ral.getText().clear();
        rml.setText("-");
        nim.setText("-");
        tvTfr.setText("-");
        tvTestPeriod.setText("-");
        costIrpef.setText("-");
        birpef.setText("-");
        irpefCl.setText("-");
        icl.setText("-");
        annual.setText("-");
        daily.setText("-");
        alertMinRml.setText("");
        tvRalInpsCa.setText("-");
        alertImg.setImageResource(0);
        btDetails.setVisibility(View.INVISIBLE);
    }*/

/*    public void clearAll(View view) {

        ral.getText().clear();
        rml.setText("-");
        nim.setText("-");
        tvTfr.setText("-");
        tvTestPeriod.setText("-");
        costIrpef.setText("-");
        birpef.setText("-");
        irpefCl.setText("-");
        icl.setText("-");
        annual.setText("-");
        daily.setText("-");
        alertMinRml.setText("");
        tvRalInpsCa.setText("-");
        alertImg.setImageResource(0);
        btDetails.setVisibility(View.INVISIBLE);
    }*/

/*    public void goDetails(View view) {
        Intent goDetails = new Intent(MainActivity.this, DetailsActivity.class);
        goDetails.putExtra("ral",ral.getText().toString());
        goDetails.putExtra("rml",rml.getText().toString());
        goDetails.putExtra("level",level.getSelectedItem().toString());
        goDetails.putExtra("alertMinRml",alertMinRml.getText().toString());
        goDetails.putExtra("nim",nim.getText().toString());
        goDetails.putExtra("tfr",tvTfr.getText().toString());
        goDetails.putExtra("pp",tvTestPeriod.getText().toString());
        goDetails.putExtra("costIrpef",costIrpef.getText().toString());
        goDetails.putExtra("baseIrpef",birpef.getText().toString());
        goDetails.putExtra("irpefCl",irpefCl.getText().toString());
        goDetails.putExtra("daily",daily.getText().toString());
        MainActivity.this.startActivity(goDetails);
    }*/

    public void goGuide(MenuItem item) {
        Intent goGuide = new Intent(MainActivity.this, GuideActivity.class);
        MainActivity.this.startActivity(goGuide);
    }

    public void goSettings(MenuItem item) {
        Intent goSettings = new Intent(MainActivity.this, SettingsActivity.class);
        MainActivity.this.startActivity(goSettings);
    }

/*    public void goWorkersList(MenuItem item) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        Intent goWorkersList = new Intent(MainActivity.this, WorkersActivity.class);
        MainActivity.this.startActivity(goWorkersList);
        drawer.closeDrawer(GravityCompat.START);
    }*/

/*    public void goWorkersListView(View view) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        Intent goWorkersList = new Intent(MainActivity.this, WorkersActivity.class);
        MainActivity.this.startActivity(goWorkersList);
        drawer.closeDrawer(GravityCompat.START);
    }*/

/*    public void goInsertWorker(MenuItem item) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        Intent goInsertWorker = new Intent(MainActivity.this, InsertWorkerActivity.class);
        MainActivity.this.startActivity(goInsertWorker);
        drawer.closeDrawer(GravityCompat.START);
    }*/

    public void logout(MenuItem item) {
        mAuth.signOut();
        updateUi();
    }

    private void updateUi() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            Intent intToLogin = new Intent(this,LoginActivity.class);
            finish();
            startActivity(intToLogin);
        }
    }

    public void goSetParams(MenuItem item) {
        Intent i = new Intent(MainActivity.this, SetParamsActivity.class);
        startActivity(i);
    }
}