package com.app.hrcosts;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class InsertWorkerActivity extends AppCompatActivity {

    EditText firstname;
    EditText lastname;
    EditText phone;
    EditText email;
    EditText ral;
    Spinner spContract;
    Spinner spLevel;
    //Button btSave;
    FirebaseDatabase mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_worker);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.insert_worker);
        initFirebase();
        initUI();
    }

    private void initUI() {
        firstname = findViewById(R.id.et_worker_name);
        lastname = findViewById(R.id.et_worker_surname);
        phone = findViewById(R.id.et_worker_phone);
        email = findViewById(R.id.et_worker_email);
        ral = findViewById(R.id.et_worker_ral);
        spContract = findViewById(R.id.sp_contract);
        spLevel = findViewById(R.id.sp_level);
        //btSave = findViewById(R.id.bt_worker_save);
    }

    private void initFirebase() {
       mDatabase = FirebaseDatabase.getInstance("https://hrcosts-default-rtdb.europe-west1.firebasedatabase.app/");
    }

    public void saveWorkerToFb(View view) {
        //Log.i("click", "saveWorkerToFb: ");
        // Write a message to the database
        if (ral.getText().toString().length() == 0 ) {
            ral.setText("0");
        }

        String firstnameStr = String.valueOf(firstname.getText());
        String lastnameStr = String.valueOf(lastname.getText());
        String phoneStr = String.valueOf(phone.getText());
        String emailStr = String.valueOf(email.getText());
        String ralStr = String.valueOf(ral.getText());
        String contract = spContract.getSelectedItem().toString();
        String level = spLevel.getSelectedItem().toString();

        int ralInt = Integer.parseInt(String.valueOf(ralStr));

        if (!firstnameValid(firstnameStr)) {
            Toast.makeText(this, "Nome non valido", Toast.LENGTH_SHORT).show();
        } else if (!lastnameValid(lastnameStr)) {
            Toast.makeText(this, "Cognome non valido", Toast.LENGTH_SHORT).show();
        } else if (!phoneValid(phoneStr)) {
            Toast.makeText(this, "Telefono non valido", Toast.LENGTH_SHORT).show();
        } else if (!emailValid(emailStr)) {
            Toast.makeText(this, "Email non valida", Toast.LENGTH_SHORT).show();
        }  else if (ralValid(ralInt)) {
            Toast.makeText(this, "RAL troppo basso", Toast.LENGTH_SHORT).show();
        } else {

            DatabaseReference myRef = mDatabase.getReference();

            myRef.child("Workers").child(lastname.getText().toString()).child("Firstname").setValue(firstname.getText().toString());
            myRef.child("Workers").child(lastname.getText().toString()).child("Lastname").setValue(lastname.getText().toString());
            myRef.child("Workers").child(lastname.getText().toString()).child("Phone").setValue(phone.getText().toString());
            myRef.child("Workers").child(lastname.getText().toString()).child("Email").setValue(email.getText().toString());
            myRef.child("Workers").child(lastname.getText().toString()).child("Ral").setValue(ral.getText().toString());
            myRef.child("Workers").child(lastname.getText().toString()).child("Contract").setValue(contract);
            myRef.child("Workers").child(lastname.getText().toString()).child("Level").setValue(level);

            this.finish();

            Toast.makeText(this, "Lavoratore inserito correttamente", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void clearFields(View view) {

        initUI();

        firstname.getText().clear();
        lastname.getText().clear();
        phone.getText().clear();
        email.getText().clear();
        ral.getText().clear();
    }

    public void saveWorkerToDb(View view) {

        initUI();
        if (ral.getText().toString().length() == 0 ) {
            ral.setText("0");
        }

        String firstnameStr = String.valueOf(firstname.getText());
        String lastnameStr = String.valueOf(lastname.getText());
        String phoneStr = String.valueOf(phone.getText());
        String emailStr = String.valueOf(email.getText());
        String ralStr = String.valueOf(ral.getText());

        int ralInt = Integer.parseInt(String.valueOf(ralStr));

        if (!firstnameValid(firstnameStr)) {
            Toast.makeText(this, "Nome non valido", Toast.LENGTH_SHORT).show();
        } else if (!lastnameValid(lastnameStr)) {
            Toast.makeText(this, "Cognome non valido", Toast.LENGTH_SHORT).show();
        } else if (!phoneValid(phoneStr)) {
            Toast.makeText(this, "Telefono non valido", Toast.LENGTH_SHORT).show();
        } else if (!emailValid(emailStr)) {
            Toast.makeText(this, "Email non valida", Toast.LENGTH_SHORT).show();
        }  else if (ralValid(ralInt)) {
            Toast.makeText(this, "RAL troppo basso", Toast.LENGTH_SHORT).show();
        } else {
            SQLiteDatabase workers = this.openOrCreateDatabase("workers", MODE_PRIVATE, null);

            //workers.execSQL("DROP TABLE workers");
            workers.execSQL("CREATE TABLE IF NOT EXISTS workers (firstname VARCHAR, lastname VARCHAR, phone INT(10), email VARCHAR, ral INT(7), contract VARCHAR, level VARCHAR)");
            workers.execSQL("INSERT INTO workers (firstname,lastname,phone,email,ral,contract,level) VALUES ('" + firstnameStr + "', '" + lastnameStr + "', '" + phoneStr + "', '" + emailStr + "', '" + ralStr + "')");

            try (Cursor c = workers.rawQuery("SELECT * FROM workers", null)) {

                int firstnameIndex = c.getColumnIndex("firstname");
                int lastnameIndex = c.getColumnIndex("lastname");
                int phoneIndex = c.getColumnIndex("phone");
                int emailIndex = c.getColumnIndex("email");
                int ralIndex = c.getColumnIndex("ral");

                c.moveToFirst();

                do {
                    Log.i("firstname", c.getString(firstnameIndex));
                    Log.i("lastname", c.getString(lastnameIndex));
                    Log.i("phone", c.getString(phoneIndex));
                    Log.i("email", c.getString(emailIndex));
                    Log.i("ral", c.getString(ralIndex));
                    //Log.i("lastname", Integer.toString(c.getInt(lastnameIndex)));
                } while (c.moveToNext());
            }
            this.finish();

            Toast.makeText(this, "Lavoratore inserito correttamente", Toast.LENGTH_SHORT).show();
        }
    }
    private boolean firstnameValid (String firstname) {
        return (firstname.length() != 0) && (firstname.length() >= 2);
    }
    private boolean lastnameValid (String lastname) {
        return (lastname.length() != 0) && (lastname.length() >= 2);
    }
    private boolean phoneValid (String phone) {
        return (phone.length() != 0) && (phone.length() >= 5);
    }
    private boolean emailValid (String email) {
        return email.contains("@") && email.contains(".");
    }
    private boolean ralValid (Integer ralInt) {
        return (ralInt  < 8000);
    }
}