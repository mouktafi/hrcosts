package com.app.hrcosts;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.text.DecimalFormat;

public class DetailWorkerActivity extends AppCompatActivity {

    SharedPreferences spSell30,spSell60,spSell90,spSell120;

    TextView tvRal,tvName,tvContractLevel,tvRml,tvNim,tvTfr,tvTestPeriod,tvCostIrpef,tvBaseIrpef,tvIrpefCl,tvInpsCl,tvRalInpsCa,tvAnnualCost,tvDailyCost,tvMinSell,tvMinSell60,tvMinSell90,tvMinSell120,tvMargin30,tvMargin60,tvMargin90,tvMargin120;

    String ral,contract,rmlStr,nimStr,tfrStr,level,inpsClStr;
    String testPeriod = "0";
    String sell30Str,sell60Str,sell90Str,sell120Str;

    double fissoDetrazione,baseIrpef,irpefClInt;
    double inpsClDb;
    double ralInpsCaDouble = 0;
    double annualCost,dailyCost;
    double minSell;
    double minSell60;
    double minSell90;
    double minSell120;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_worker);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        InitBundle();
        GetSP();

        CalcCosts();

        InitUi();
        CalcTestPeriod();
    }

    private void CalcCosts() {

        double ralDouble = Double.parseDouble(ral);
        double rmlDouble = ralDouble / 14;
        rmlStr = new DecimalFormat("##.##").format(rmlDouble);

        double irpef = 0;
        fissoDetrazione = 0;

        if (ralDouble >= 999 && ralDouble <= 15000) {
            irpef = (0.23);
        }else if (ralDouble >= 15001 && ralDouble <= 28000) {
            irpef = (0.27);
            fissoDetrazione = 600;
        }else if (ralDouble >= 28001 && ralDouble <= 55000) {
            irpef = (0.38);
            fissoDetrazione = 3680;
        }else if (ralDouble >= 55001 && ralDouble <= 75000) {
            irpef = (0.41);
            fissoDetrazione = 5330;
        }else if (ralDouble >= 75001) {
            irpef = (0.43);
            fissoDetrazione = 6830;
        }

        inpsClDb = ralDouble * 0.0949;
        baseIrpef = ralDouble - inpsClDb;
        irpefClInt = (baseIrpef * irpef) - fissoDetrazione;
        double nimDouble = (baseIrpef-irpefClInt)/14;
        double tfrDouble = ralDouble / 13.5;

        switch (contract) {
            case "Indeterminato":
                ralInpsCaDouble = ralDouble * 1.2936;
                break;
            case "Determinato":
                ralInpsCaDouble = ralDouble * 1.3076;
                break;
            case "Apprendistato":
                ralInpsCaDouble = ralDouble * 1.1;
        }

        annualCost = (ralInpsCaDouble + tfrDouble);
        dailyCost = annualCost / 210;

        minSell = dailyCost * Double.parseDouble(sell30Str)/100 + dailyCost;
        minSell60 = dailyCost * Double.parseDouble(sell60Str)/100 + dailyCost;
        minSell90 = dailyCost * Double.parseDouble(sell90Str)/100 + dailyCost;
        minSell120 = dailyCost * Double.parseDouble(sell120Str)/100 + dailyCost;

        nimStr = new DecimalFormat("##.##").format(nimDouble);
        tfrStr = new DecimalFormat("##.##").format(tfrDouble);
        inpsClStr = new DecimalFormat("##.##").format(inpsClDb);
    }

    private void GetSP() {

        spSell30 = this.getSharedPreferences("sell30", Context.MODE_PRIVATE);
        sell30Str = spSell30.getString("sell30", "25");

        spSell60 = this.getSharedPreferences("sell60", Context.MODE_PRIVATE);
        sell60Str = spSell60.getString("sell60", "30");

        spSell90 = this.getSharedPreferences("sell90", Context.MODE_PRIVATE);
        sell90Str = spSell90.getString("sell90", "35");

        spSell120 = this.getSharedPreferences("sell120", Context.MODE_PRIVATE);
        sell120Str = spSell120.getString("sell120", "40");
    }

    private void InitBundle(){
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        String name = bundle.getString("name");
        ral = bundle.getString("ral");
        contract = bundle.getString("contract");
        level = bundle.getString("level");
        setTitle(name);
    }

    private void CalcTestPeriod() {

        switch (level) {
            case "Quadro":
            case "1":
                testPeriod = "6 mesi CC";
                break;
            case "2":
            case "3":
            case "4":
            case "5":
                testPeriod = "60 giorni LL";
                break;
            case "6":
            case "7":
                testPeriod = "45 giorni LL";
                break;
        }
        tvTestPeriod.setText(testPeriod);
    }

    private void InitUi() {
        tvName = findViewById(R.id.lb_ral_worker_detail);
        tvRal = findViewById(R.id.tv_ral_detail);
        tvContractLevel =findViewById(R.id.tv_contract);
        tvRml = findViewById(R.id.tv_det_rml);
        tvNim = findViewById(R.id.tv_det_nim);
        tvTfr = findViewById(R.id.tv_det_tfr);
        tvTestPeriod = findViewById(R.id.tv_det_test_period);
        tvCostIrpef = findViewById(R.id.tv_det_cost_irpef);
        tvBaseIrpef = findViewById(R.id.tv_det_base_irpef);
        tvIrpefCl = findViewById(R.id.tv_det_irpef_cl);
        tvInpsCl = findViewById(R.id.tv_det_inps_cl);
        tvRalInpsCa = findViewById(R.id.tv_det_inps_ca);
        tvAnnualCost = findViewById(R.id.tv_annual_cost);
        tvDailyCost = findViewById(R.id.tv_daily_cost);

        tvMinSell = findViewById(R.id.tv_min_sell);
        tvMinSell60 = findViewById(R.id.tv_min_sell_60);
        tvMinSell90 = findViewById(R.id.tv_min_sell_90);
        tvMinSell120 = findViewById(R.id.tv_min_sell_120);

        tvMargin30 = findViewById(R.id.tv_margin_30);
        tvMargin60 = findViewById(R.id.tv_margin_60);
        tvMargin90 = findViewById(R.id.tv_margin_90);
        tvMargin120 = findViewById(R.id.tv_margin_120);

        tvRal.setText(ral);
        //tvContract.setText(contract);
        tvContractLevel.setText(String.format("%s livello %s", contract, level));
        tvRml.setText(rmlStr);
        tvNim.setText(nimStr);
        tvTfr.setText(tfrStr);

        String fissoDetrazioneStr = String.valueOf(fissoDetrazione);
        tvCostIrpef.setText(fissoDetrazioneStr);

        String baseIrpefStr = String.valueOf(baseIrpef);
        tvBaseIrpef.setText(baseIrpefStr);

        //String rmlFormat = new DecimalFormat("##.##").format(rmlDouble);

        String irpefClIntStr = String.valueOf(irpefClInt);
        tvIrpefCl.setText(irpefClIntStr);

        tvInpsCl.setText(inpsClStr);

        tvRalInpsCa.setText(new DecimalFormat("##.##").format(ralInpsCaDouble));

        tvAnnualCost.setText(new DecimalFormat("##.##").format(annualCost));
        tvDailyCost.setText(new DecimalFormat("##.##").format(dailyCost));

        tvMinSell.setText(new DecimalFormat("##.##").format(minSell));
        tvMinSell60.setText(new DecimalFormat("##.##").format(minSell60));
        tvMinSell90.setText(new DecimalFormat("##.##").format(minSell90));
        tvMinSell120.setText(new DecimalFormat("##.##").format(minSell120));

        tvMargin30.setText(String.format("%s%%", sell30Str));
        tvMargin60.setText(String.format("%s%%", sell60Str));
        tvMargin90.setText(String.format("%s%%", sell90Str));
        tvMargin120.setText(String.format("%s%%", sell120Str));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.detail_worker_menu, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        GetSP();
        CalcCosts();
        InitUi();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void goSetParams(MenuItem item) {
        Intent i = new Intent(DetailWorkerActivity.this, SetParamsActivity.class);
        startActivity(i);
    }
}