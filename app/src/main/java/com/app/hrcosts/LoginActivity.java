package com.app.hrcosts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "Login Activity";
    EditText mUsername;
    EditText mPassword;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Login");

        mAuth = FirebaseAuth.getInstance();

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser currentUser) {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            //String name = user.getDisplayName();
            String email = user.getEmail();
            //Uri photourl = user.getPhotoUrl();

            Intent intent = new Intent(this,MainActivity.class);
            intent.putExtra("username", email);
            finish();
            startActivity(intent);

           /* boolean emailVerified = user.isEmailVerified();
            String uid = user.getUid();*/
        }

    }

    public void login(View view) {
        Log.d("login", "clock ok");

        mUsername = findViewById(R.id.et_login_username);
        mPassword = findViewById(R.id.et_login_password);

        String username = mUsername.getText().toString();
        String password = mPassword.getText().toString();

        Log.d("credentials", username + password);

        if (!(username.length() > 2) || !(username.contains("@") || !(username.contains(".")))) {
            Toast.makeText(LoginActivity.this,"Formato email non valido", Toast.LENGTH_SHORT).show();
        } else if (!(password.length() > 2)) {
            Toast.makeText(LoginActivity.this,"Formato password non valido", Toast.LENGTH_SHORT).show();
        } else {
            loginUser(username, password);
        }

    }

    private void loginUser(String email, String password) {
        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "Login OK");
                    FirebaseUser user = mAuth.getCurrentUser();
                    updateUI(user);
                } else {
                    Toast.makeText(LoginActivity.this,"Credenziali errate", Toast.LENGTH_SHORT).show();
                    //updateUI(null);
                }
            }
        });
    }
}