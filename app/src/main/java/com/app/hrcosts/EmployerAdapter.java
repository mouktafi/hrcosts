package com.app.hrcosts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class EmployerAdapter extends RecyclerView.Adapter<EmployerAdapter.MyViewHolder> {

    Context context;
    ArrayList<Employer> list;


    public EmployerAdapter(Context context, ArrayList<Employer> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.employer,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Employer employer = list.get(position);
        holder.firstname.setText(employer.getFirstname());
        holder.lastname.setText(employer.getLastname());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView firstname, lastname;
        public MyViewHolder (@NonNull View itemView) {
            super(itemView);

            firstname = itemView.findViewById(R.id.tvfirstName);
            lastname = itemView.findViewById(R.id.tvlastName);
        }
     }

}
