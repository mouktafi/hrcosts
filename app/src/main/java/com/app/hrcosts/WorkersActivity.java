package com.app.hrcosts;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class WorkersActivity extends AppCompatActivity {

    private FirebaseDatabase mDatabase;
    SearchView svWorker;
    ArrayAdapter<String> arrayAdapter;
    ListView listView;
    ProgressDialog progressDialog;
    FloatingActionButton fabAddWorker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workers);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.workers_list);

        initFirebase();
        retrieveWorkers();

/*        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(() -> {
            retrieveWorkers(); // your code
            pullToRefresh.setRefreshing(false);
        });*/

        fabAddWorker = findViewById(R.id.fab_add_worker);
        fabAddWorker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WorkersActivity.this, InsertWorkerActivity.class));
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        assert cm != null;
        NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        return (wifi != null && wifi.isConnected()) || (mobile != null && mobile.isConnected());
    }

    private void initFirebase() {
        mDatabase = FirebaseDatabase.getInstance("https://hrcosts-default-rtdb.europe-west1.firebasedatabase.app/");
    }

    public void closeDialog(){
        progressDialog.dismiss();
    }
    public void createDialog(){
        progressDialog = new ProgressDialog(this);
        if (!isNetworkAvailable()) {
            progressDialog.setTitle("Rete assente");
            progressDialog.setMessage("Connettere il dispositivo ad una rete");
            progressDialog.setCancelable(true);
        } else {
            progressDialog.setTitle("Attendi");
            progressDialog.setMessage("Caricamento lista dipendenti");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
    }

    private void retrieveWorkers() {

        createDialog();

        DatabaseReference myRef = mDatabase.getReference("Workers");
        ArrayList<String> mWorker = new ArrayList<>();
        ArrayList<String> mName = new ArrayList<>();
        ArrayList<String> mRal = new ArrayList<>();
        ArrayList<String> mContract = new ArrayList<>();
        ArrayList<String> mLevel = new ArrayList<>();
        listView = findViewById(R.id.lv_workers);
        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1, mWorker);
        listView.setAdapter(arrayAdapter);


        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                String firstname = snapshot.child("Firstname").getValue(String.class);
                String lastname = snapshot.child("Lastname").getValue(String.class);
                String email = snapshot.child("Email").getValue(String.class);
                String ral = snapshot.child("Ral").getValue(String.class);
                String contract = snapshot.child("Contract").getValue(String.class);
                String level = snapshot.child("Level").getValue(String.class);

                //mWorker.add(firstname+" "+lastname+"\n"+email+"\n"+ral+" €");
                mWorker.add(firstname+" "+lastname);
                mName.add(firstname+" "+lastname);
                mRal.add(ral);
                mContract.add(contract);
                mLevel.add(level);
                arrayAdapter.notifyDataSetChanged();

                svWorker = findViewById(R.id.sv_worker);
                svWorker.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        WorkersActivity.this.arrayAdapter.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        WorkersActivity.this.arrayAdapter.getFilter().filter(newText);
                        return false;
                    }
                });

                listView.setOnItemClickListener((parent, view, position, id) -> {

                    //String workerDetails = listView.getItemAtPosition(position).toString();
                    String namePassed = mName.get(position);
                    String ralPassed = mRal.get(position);
                    String contractPassed = mContract.get(position);
                    String levelPassed = mLevel.get(position);
                    //Log.i("details", String.valueOf(workerDetails));

                    //Map<String, Object> value = (Map<String, Object>) snapshot.getValue();

                    //Log.i("details", String.valueOf(ralPassed));
                    Intent goDetailWorkerActivity = new Intent(WorkersActivity.this, DetailWorkerActivity.class);
                    goDetailWorkerActivity.putExtra("name",namePassed);
                    goDetailWorkerActivity.putExtra("ral",ralPassed);
                    goDetailWorkerActivity.putExtra("contract",contractPassed);
                    goDetailWorkerActivity.putExtra("level",levelPassed);
                    WorkersActivity.this.startActivity(goDetailWorkerActivity);
                    finish();
                });

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w("failed", "Failed to read value.", error.toException());
            }
        });

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                closeDialog();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                closeDialog();
            }
        });


    }
}