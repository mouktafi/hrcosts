package com.app.hrcosts;

class Employer {

    String firstname;
    String lastname;
    String age;

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAge() {
        return age;
    }
}
