package com.app.hrcosts;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.Objects;

public class DetailsActivity extends AppCompatActivity {

    SharedPreferences spSell30,spSell60,spSell90,spSell120;

    String level,nim,rml,tfr,testPeriod,costIrpef,birpef,irpefCl,inpsCl,inpsCa,annualStr,dailyStr;
    String sell30Str,sell60Str,sell90Str,sell120Str;
    String minSellFormat,minSell60Format,minSell90Format,minSell120Format;
    double rmlMin = 0;
    double rmlDouble,dailyDouble;
    double minSell,minSell60,minSell90,minSell120;

    //TextView tvRal;
    TextView tvRml;
    ImageView ivImgAlert;
    TextView tvAlertMinRml;
    TextView tvNim;
    TextView tvTfr;
    TextView tvTestPeriod;
    TextView tvCostIrpef;
    TextView tvBaseIrpef;
    TextView tvIrpefCl;
    TextView tvInpsCl;
    TextView tvInpsCa;
    TextView tvMinSell;
    TextView tvMinSell60;
    TextView tvMinSell90;
    TextView tvMinSell120;
    TextView tvAnnual,tvDaily;
    TextView tvMargin30,tvMargin60,tvMargin90,tvMargin120;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        GetSP();

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        String ral = bundle.getString("ral");

        rml = bundle.getString("rml");
        assert rml != null;
        rmlDouble = Double.parseDouble(rml.replace(',','.'));
        level = bundle.getString("level");
        nim = bundle.getString("nim");
        tfr = bundle.getString("tfr");
        testPeriod = bundle.getString("pp");
        costIrpef = bundle.getString("costIrpef");
        birpef = bundle.getString("baseIrpef");
        irpefCl = bundle.getString("irpefCl");
        inpsCl = bundle.getString("inpsCl");
        inpsCa = bundle.getString("inpsCa");
        annualStr = bundle.getString("annual");
        dailyStr = bundle.getString("daily");
        assert dailyStr != null;
        dailyDouble = Double.parseDouble(dailyStr.replace(',','.'));

        setTitle("Liv. "+level+" | RAL "+ral);

        CalcCosts();
        InitUi();
    }

    private void CalcCosts(){
        minSell = dailyDouble * Double.parseDouble(sell30Str)/100 + dailyDouble;
        minSell60 = dailyDouble * Double.parseDouble(sell60Str)/100 + dailyDouble;
        minSell90 = dailyDouble * Double.parseDouble(sell90Str)/100 + dailyDouble;
        minSell120 = dailyDouble * Double.parseDouble(sell120Str)/100 + dailyDouble;

        minSellFormat = new DecimalFormat("##.##").format(minSell);
        minSell60Format = new DecimalFormat("##.##").format(minSell60);
        minSell90Format = new DecimalFormat("##.##").format(minSell90);
        minSell120Format = new DecimalFormat("##.##").format(minSell120);
    }

    private void InitUi(){
        tvRml = findViewById(R.id.tv_det_rml);
        ivImgAlert = findViewById(R.id.iv_det_img_alert);
        tvAlertMinRml = findViewById(R.id.tv_det_alert_txt);
        tvNim = findViewById(R.id.tv_det_nim);
        tvTfr = findViewById(R.id.tv_det_tfr);
        tvTestPeriod = findViewById(R.id.tv_det_test_period);
        tvCostIrpef = findViewById(R.id.tv_det_cost_irpef);
        tvBaseIrpef = findViewById(R.id.tv_det_base_irpef);
        tvIrpefCl = findViewById(R.id.tv_det_irpef_cl);
        tvInpsCl = findViewById(R.id.tv_det_inps_cl);
        tvInpsCa = findViewById(R.id.tv_det_inps_ca);
        tvAnnual = findViewById(R.id.tv_annual_cost);
        tvDaily = findViewById(R.id.tv_daily_cost);
        tvMinSell = findViewById(R.id.tv_min_sell);
        tvMinSell60 = findViewById(R.id.tv_min_sell_60);
        tvMinSell90 = findViewById(R.id.tv_min_sell_90);
        tvMinSell120 = findViewById(R.id.tv_min_sell_120);

        tvMargin30 = findViewById(R.id.tv_margin_30);
        tvMargin60 = findViewById(R.id.tv_margin_60);
        tvMargin90 = findViewById(R.id.tv_margin_90);
        tvMargin120 = findViewById(R.id.tv_margin_120);

        switch (Objects.requireNonNull(level)) {
            case "Quadro":
                rmlMin = 2699.84;
                break;
            case "1":
                rmlMin = 2248.08;
                break;
            case "2":
                rmlMin = 2012.44;
                break;
            case "3":
                rmlMin = 1793.11;
                break;
            case "4":
                rmlMin = 1618.75;
                break;
            case "5":
                rmlMin = 1510.98;
                break;
            case "6":
                rmlMin = 1407.90;
                break;
            case "7":
                rmlMin = 1283.36;
                break;
        }


        if (rmlDouble < rmlMin) {
            ivImgAlert.setImageResource(R.drawable.ic_menu_alert);
            ivImgAlert.setColorFilter(getResources().getColor(R.color.error));
            tvAlertMinRml.setText(String.format("%s%s", getString(R.string.rmldetmin), rmlMin));
        } else {
            ivImgAlert.setImageResource(R.drawable.ic_menu_ok);
            ivImgAlert.setColorFilter(getResources().getColor(R.color.teal_700));
            tvAlertMinRml.setText(String.format("%s%s", getString(R.string.rmldetok), rmlMin));
        }
        tvRml.setText(rml);
        tvNim.setText(nim);
        tvTfr.setText(tfr);
        switch (testPeriod) {
            case "6 mesi CC":
                tvTestPeriod.setText(R.string.sixmc);
                break;
            case "60 giorni LL":
                tvTestPeriod.setText(R.string.twml);
                break;
            case "45 giorni LL":
                tvTestPeriod.setText(R.string.ffml);
                break;
        }
        tvCostIrpef.setText(costIrpef);
        tvBaseIrpef.setText(birpef);
        tvIrpefCl.setText(irpefCl);
        tvInpsCl.setText(inpsCl);
        tvInpsCa.setText(inpsCa);
        tvAnnual.setText(annualStr);
        tvDaily.setText(dailyStr);
        tvMinSell.setText(minSellFormat);
        tvMinSell60.setText(minSell60Format);
        tvMinSell90.setText(minSell90Format);
        tvMinSell120.setText(minSell120Format);

        tvMargin30.setText(String.format("%s%%", sell30Str));
        tvMargin60.setText(String.format("%s%%", sell60Str));
        tvMargin90.setText(String.format("%s%%", sell90Str));
        tvMargin120.setText(String.format("%s%%", sell120Str));
    }

    private void GetSP() {

        spSell30 = this.getSharedPreferences("sell30", Context.MODE_PRIVATE);
        sell30Str = spSell30.getString("sell30", "25");

        spSell60 = this.getSharedPreferences("sell60", Context.MODE_PRIVATE);
        sell60Str = spSell60.getString("sell60", "30");

        spSell90 = this.getSharedPreferences("sell90", Context.MODE_PRIVATE);
        sell90Str = spSell90.getString("sell90", "35");

        spSell120 = this.getSharedPreferences("sell120", Context.MODE_PRIVATE);
        sell120Str = spSell120.getString("sell120", "40");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.detail_worker_menu, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        GetSP();
        CalcCosts();
        InitUi();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    public void goSetParams(MenuItem item) {
        Intent i = new Intent(DetailsActivity.this, SetParamsActivity.class);
        startActivity(i);
    }
}