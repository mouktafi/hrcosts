package com.app.hrcosts;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SetParamsActivity extends AppCompatActivity {

    EditText etParam30, etParam60, etParam90,etParam120;
    String param30Str,param60Str,param90Str,param120Str,sell30Str,sell60Str,sell90Str,sell120Str;
    SharedPreferences spSellsParams30,spSellsParams60,spSellsParams90,spSellsParams120,spSell30,spSell60,spSell90,spSell120;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_params);
        setTitle(R.string.sellsParams);

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        InitUI();
    }

    private void InitUI(){
        etParam30 = findViewById(R.id.etParam30);
        etParam60 = findViewById(R.id.etParam60);
        etParam90 = findViewById(R.id.etParam90);
        etParam120 = findViewById(R.id.etParam120);
        btnSave = findViewById(R.id.btnSaveParams);

        spSell30 = this.getSharedPreferences("sell30", Context.MODE_PRIVATE);
        sell30Str = spSell30.getString("sell30", sell30Str);

        spSell60 = this.getSharedPreferences("sell60", Context.MODE_PRIVATE);
        sell60Str = spSell60.getString("sell60", sell60Str);

        spSell90 = this.getSharedPreferences("sell90", Context.MODE_PRIVATE);
        sell90Str = spSell90.getString("sell90", sell90Str);

        spSell120 = this.getSharedPreferences("sell120", Context.MODE_PRIVATE);
        sell120Str = spSell120.getString("sell120", sell120Str);

        etParam30.setText(sell30Str, TextView.BufferType.EDITABLE);
        etParam60.setText(sell60Str, TextView.BufferType.EDITABLE);
        etParam90.setText(sell90Str, TextView.BufferType.EDITABLE);
        etParam120.setText(sell120Str, TextView.BufferType.EDITABLE);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveSellsParams(view);
            }
        });
    }

    public void saveSellsParams(View view) {

        param30Str = etParam30.getText().toString();
        param60Str = etParam60.getText().toString();
        param90Str = etParam90.getText().toString();
        param120Str = etParam120.getText().toString();
        //Log.i("param30Str", param30Str);

        if (TextUtils.isEmpty(etParam30.getText().toString())){
            Toast.makeText(this, "Inserire margine a 30 gg", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(etParam60.getText().toString())){
            Toast.makeText(this, "Inserire margine a 60 gg", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(etParam90.getText().toString())){
            Toast.makeText(this, "Inserire margine a 90 gg", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(etParam120.getText().toString())){
            Toast.makeText(this, "Inserire margine a 120 gg", Toast.LENGTH_SHORT).show();
        }else{
            spSellsParams30 = this.getSharedPreferences("sell30", Context.MODE_PRIVATE);
            spSellsParams30.edit().putString("sell30", param30Str).apply();

            spSellsParams60 = this.getSharedPreferences("sell60", Context.MODE_PRIVATE);
            spSellsParams60.edit().putString("sell60", param60Str).apply();

            spSellsParams90 = this.getSharedPreferences("sell90", Context.MODE_PRIVATE);
            spSellsParams90.edit().putString("sell90", param90Str).apply();

            spSellsParams120 = this.getSharedPreferences("sell120", Context.MODE_PRIVATE);
            spSellsParams120.edit().putString("sell120", param120Str).apply();

            Toast.makeText(this, "Margini salvati", Toast.LENGTH_SHORT).show();

            this.finish();
        }
      /*  String sell30 = spSellsParams.getString("sell30", "1.26");
        String sell60 = spSellsParams.getString("sell60", "1.30");
        String sell90 = spSellsParams.getString("sell90", "1.35");
        String sell120 = spSellsParams.getString("sell120", "1.40");

        Log.i("sell30", param30Str);
        Log.i("sell60", param60Str);
        Log.i("sell90", param90Str);
        Log.i("sell120", param120Str);*/

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}