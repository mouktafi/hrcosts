package com.app.hrcosts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class EmployersActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    DatabaseReference database;
    EmployerAdapter employerAdapter;
    ArrayList<Employer> list;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employers);

        recyclerView = findViewById(R.id.rv_employers);
        database = FirebaseDatabase.getInstance("https://hrcosts-default-rtdb.europe-west1.firebasedatabase.app/").getReference("Workers");
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        list = new ArrayList<>();
        employerAdapter = new EmployerAdapter(this, list);
        recyclerView.setAdapter(employerAdapter);

        database.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    Employer employer = dataSnapshot.getValue(Employer.class);
                    list.add(employer);
                }

                employerAdapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        assert cm != null;
        NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        return (wifi != null && wifi.isConnected()) || (mobile != null && mobile.isConnected());
    }

    public void closeDialog(){
        progressDialog.dismiss();
    }
    public void createDialog(){
        progressDialog = new ProgressDialog(this);
        if (!isNetworkAvailable()) {
            progressDialog.setTitle("Rete assente");
            progressDialog.setMessage("Connettere il dispositivo ad una rete");
            progressDialog.setCancelable(true);
        } else {
            progressDialog.setTitle("Attendi");
            progressDialog.setMessage("Caricamento lista dipendenti");
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
    }

}