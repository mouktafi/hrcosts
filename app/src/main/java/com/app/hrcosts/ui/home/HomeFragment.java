package com.app.hrcosts.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.hrcosts.DetailsActivity;
import com.app.hrcosts.MainActivity;
import com.app.hrcosts.R;
import com.app.hrcosts.WorkersActivity;

import java.text.DecimalFormat;
import java.util.Objects;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    Button btnSearchWorker,btnCalc,btnReset,btnDetail;

    EditText ral;
    TextView rml;
    TextView costIrpef;
    TextView irpefCl;
    TextView inpsCl;
    TextView annual;
    TextView daily;
    TextView birpef;
    TextView nim;
    TextView tvTfr;
    TextView tvTestPeriod;
    TextView alertMinRml;
    TextView tvRalInpsCa;

    Spinner contract;
    Spinner level;
    ImageView alertImg;
    Button btDetails;

    TextWatcher tw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            clearDetail();
        }
    };

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);



        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnSearchWorker = requireView().findViewById(R.id.bt_search_worker);
        btnCalc = requireView().findViewById(R.id.bt_calculate);
        btnReset = requireView().findViewById(R.id.bt_reset);
        btnDetail = requireView().findViewById(R.id.bt_details);

        btnSearchWorker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goWorkersListView(view);
            }
        });

        btnCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CalculateCostsTi(view);
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearAll();
            }
        });

        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goDetails(view);
            }
        });

    }

    private void CalculateCostsTi(View view) {

        ral = requireView().findViewById(R.id.et_ral);

        if (ral.getText().toString().length() == 0){
            ral.setText("0");

        } else {ral.addTextChangedListener(tw);}

        double ralDouble = Double.parseDouble(ral.getText().toString());
        int ralInt = (int) ralDouble;
        String ralStr = String.valueOf(ralInt);
        double irpef = 0;
        double fissoDetrazione = 0;

        rml = requireView().findViewById(R.id.tv_rml);
        costIrpef = requireView().findViewById(R.id.tv_costanteIrpef);
        irpefCl = requireView().findViewById(R.id.tv_irpefcl);
        inpsCl = requireView().findViewById(R.id.tv_inpscl);
        annual = requireView().findViewById(R.id.tv_annual_cost);
        daily = requireView().findViewById(R.id.tv_daily_cost);
        birpef = requireView().findViewById(R.id.tv_baseirpef);
        nim = requireView().findViewById(R.id.tv_nim);
        tvTfr = requireView().findViewById(R.id.tv_tfr);
        tvTestPeriod = requireView().findViewById(R.id.tv_test_period);
        alertMinRml = requireView().findViewById(R.id.tv_alert_rml_min);
        tvRalInpsCa = requireView().findViewById(R.id.tv_ralinps_ca);
        contract = requireView().findViewById(R.id.sp_contract);
        level = requireView().findViewById(R.id.sp_level);
        alertImg = requireView().findViewById(R.id.iv_alertImg);
        btDetails = requireView().findViewById(R.id.bt_details);

        contract.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                clearDetail();
                CalculateCostsTi(view);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        level.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                clearDetail();
                CalculateCostsTi(view);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });


        if (ralInt < 8000) {
            Toast.makeText(getActivity(),"Inserire un RAL che sia degno di un lavoratore",Toast.LENGTH_SHORT).show();
            clearAll();
        } else {

            if (ralDouble >= 999 && ralDouble <= 15000) {
                irpef = (0.23);
            }else if (ralDouble >= 15001 && ralDouble <= 28000) {
                irpef = (0.27);
                fissoDetrazione = 600;
            }else if (ralDouble >= 28001 && ralDouble <= 55000) {
                irpef = (0.38);
                fissoDetrazione = 3680;
            }else if (ralDouble >= 55001 && ralDouble <= 75000) {
                irpef = (0.41);
                fissoDetrazione = 5330;
            }else if (ralDouble >= 75001) {
                irpef = (0.43);
                fissoDetrazione = 6830;
            }

            double rmlDouble = ralDouble / 14;
            double inpsClDb = ralDouble * 0.0949;
            double tfrDb = ralDouble / 13.5;
            double ralInpsCaDouble = 0;
            String contracts = contract.getSelectedItem().toString();
            switch (contracts) {
                case "Indeterminato":
                    ralInpsCaDouble = ralDouble * 1.2936;
                    break;
                case "Determinato":
                    ralInpsCaDouble = ralDouble * 1.3076;
                    break;
                case "Apprendistato":
                    ralInpsCaDouble = ralDouble * 1.1;
            }
            //double ralInpsCaDouble = ralDouble * 1.2936;
            //double annualCost = (ralDouble * 1.39);
            double annualCostDb = (ralInpsCaDouble + tfrDb);
            double dailyCostDb = annualCostDb / 210;
            double baseIrpefDb = ralDouble - inpsClDb;
            double irpefClDb = (baseIrpefDb * irpef) - fissoDetrazione;
            double nimDb = (baseIrpefDb-irpefClDb)/14;
            double rmlMin = 0;


            String rmlFormat = new DecimalFormat("##.##").format(rmlDouble);
            String inpsClFormat = new DecimalFormat("##.##").format(inpsClDb);
            String annualCostFormat = new DecimalFormat("##.##").format(annualCostDb);
            String dailyCostFormat = new DecimalFormat("##.##").format(dailyCostDb);
            String baseIrpefFormat = new DecimalFormat("##.##").format(baseIrpefDb);
            String irpefClFormat = new DecimalFormat("##.##").format(irpefClDb);
            String nimFormat = new DecimalFormat("##.##").format(nimDb);
            String tfrFormat = new DecimalFormat("##.##").format(tfrDb);
            String ralInpsCaFormat = new DecimalFormat("##.##").format(ralInpsCaDouble);

            String levels = level.getSelectedItem().toString();

            String fissoDetrazioneStr = String.valueOf(fissoDetrazione);

            switch (levels) {
                case "Quadro":
                    rmlMin = 2699.84;
                    break;
                case "1":
                    rmlMin = 2248.08;
                    break;
                case "2":
                    rmlMin = 2012.44;
                    break;
                case "3":
                    rmlMin = 1793.11;
                    break;
                case "4":
                    rmlMin = 1618.75;
                    break;
                case "5":
                    rmlMin = 1510.98;
                    break;
                case "6":
                    rmlMin = 1407.90;
                    break;
                case "7":
                    rmlMin = 1283.36;
                    break;
            }
            String testPeriod = "0";
            switch (levels) {
                case "Quadro":
                case "1":
                    testPeriod = "6 mesi CC";
                    break;
                case "2":
                case "3":
                case "4":
                case "5":
                    testPeriod = "60 giorni LL";
                    break;
                case "6":
                case "7":
                    testPeriod = "45 giorni LL";
                    break;
            }

            if (rmlDouble < rmlMin) {
                alertImg.setImageResource(R.drawable.ic_menu_alert);
                alertImg.setColorFilter(getResources().getColor(R.color.error));
                alertMinRml.setText(String.format("%s%s", getString(R.string.rmlmin), rmlMin));
            } else {
                alertImg.setImageResource(R.drawable.ic_menu_ok);
                alertImg.setColorFilter(getResources().getColor(R.color.teal_700));
                alertMinRml.setText(String.format("%s%s", getString(R.string.rmlok), rmlMin));
            }

            rml.setText(rmlFormat);
            costIrpef.setText(fissoDetrazioneStr);
            annual.setText(annualCostFormat);
            daily.setText(dailyCostFormat);
            birpef.setText(baseIrpefFormat);
            irpefCl.setText(irpefClFormat);
            inpsCl.setText(inpsClFormat);
            tvRalInpsCa.setText(ralInpsCaFormat);
            nim.setText(nimFormat);
            tvRalInpsCa.setText(ralInpsCaFormat);
            tvTfr.setText(tfrFormat);
            tvTestPeriod.setText(testPeriod);

            Toast.makeText(getActivity(),"Dati aggiornati",Toast.LENGTH_SHORT).show();
            /*Snackbar.make(view, "Dati aggiornati", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();*/

            //Log.i("info", fissoDetrazioneStr);

            btDetails.setVisibility(View.VISIBLE);
        }
    }

    private void clearDetail() {
        rml.setText("-");
        nim.setText("-");
        tvTfr.setText("-");
        tvTestPeriod.setText("-");
        costIrpef.setText("-");
        birpef.setText("-");
        irpefCl.setText("-");
        inpsCl.setText("-");
        tvRalInpsCa.setText("-");
        annual.setText("-");
        daily.setText("-");
        alertMinRml.setText("");
        tvRalInpsCa.setText("-");
        alertImg.setImageResource(0);
        btDetails.setVisibility(View.INVISIBLE);
    }

    private void clearAll() {
        ral.getText().clear();
        rml.setText("-");
        nim.setText("-");
        tvTfr.setText("-");
        tvTestPeriod.setText("-");
        costIrpef.setText("-");
        birpef.setText("-");
        irpefCl.setText("-");
        inpsCl.setText("-");
        tvRalInpsCa.setText("-");
        annual.setText("-");
        daily.setText("-");
        alertMinRml.setText("");
        tvRalInpsCa.setText("-");
        alertImg.setImageResource(0);
        btDetails.setVisibility(View.INVISIBLE);
    }

    public void goWorkersListView(View view) {
        Intent intent = new Intent(getActivity(), WorkersActivity.class);
        startActivity(intent);
    }

    public void goDetails(View view) {
        Intent i = new Intent(getActivity(), DetailsActivity.class);
        i.putExtra("ral",ral.getText().toString());
        i.putExtra("rml",rml.getText().toString());
        i.putExtra("level",level.getSelectedItem().toString());
        i.putExtra("alertMinRml",alertMinRml.getText().toString());
        i.putExtra("nim",nim.getText().toString());
        i.putExtra("tfr",tvTfr.getText().toString());
        i.putExtra("pp",tvTestPeriod.getText().toString());
        i.putExtra("costIrpef",costIrpef.getText().toString());
        i.putExtra("baseIrpef",birpef.getText().toString());
        i.putExtra("irpefCl",irpefCl.getText().toString());
        i.putExtra("inpsCl",inpsCl.getText().toString());
        i.putExtra("inpsCa",tvRalInpsCa.getText().toString());
        i.putExtra("annual",annual.getText().toString());
        i.putExtra("daily",daily.getText().toString());

        HomeFragment.this.startActivity(i);
    }
}